# Introduction

Let's say you have your organization and you have chosen to use Google's Cloud Platform (GCP).
You know its not a great idea to manage all of your resources manually in the UI and
you have heard of tools like Terraform, but you don't know where to start.

This is the project for you!

First, make sure you have a [Gitlab group](https://docs.gitlab.com/ee/user/group/) setup for your organization.

Next, get Cookiecutter:

    pip install "cookiecutter>=1.4.0"

In the example below, installation is as easy as running `cookiecutter` against this repository.  Cookicutter will take care of cloning and creating directories.  You'll be prompted for some values. Provide them, then you will be ready to begin.
Answer the prompts with your own desired options_. For example::

    $ cookiecutter https://gitlab.com/pennatus/gcp-cookiecutter.git
    org_name [MyAcmeCoNoSpaces]: MyAcmeCo
    org_domain_name [myorg.com]: myorg.com
    org_project_name [is-org]: is-org
    gitlab_group[my-gitlab-group]: my-gitlab-group
    project_slug[my_acme_co]: my_acme_co

Enter the project and take a look around::

    cd my_acme_co/
    ls

Create a git repo and push it there::

    git init
    git add .
    git commit -m "first awesome commit"
    git remote add origin git@gitlab.com:my-gitlab-group/my_acme_co.git
    git push -u origin master

The step by step instructions to setup your organization can be found in the `README.md` in the new project.
