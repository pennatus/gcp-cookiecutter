output "vault_url" {
  value = module.vault_project.vault_url
  description = "This is the url GCP assigns to access the Vault Cloud Run instance"
}

output "vault_project_id" {
  value = module.vault_project.vault_project_id
  description = "This is project id that is hosting vault"
}

output "vault_project_number" {
  value = module.vault_project.vault_project_number
  description = "This is project number that is hosting vault"
}

output "org_id" {
  value = module.vault_project.org_id
  description = "This is organization id"
}