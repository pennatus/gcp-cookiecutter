# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be supplied and are secretive
# ---------------------------------------------------------------------------------------------------------------------

variable "GCP_ACCESS_TOKEN" {
  description = "Short lived GCP access token.  Get using `gcloud auth print-access-token`"
  type        = string
}


# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters can be specified to override defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "DOMAIN" {
  description = "This is the domain for which your GCP account is hosted under.  Supplied by CookieCutter template."
  type        = string
}

variable "BILLING_NAME_GENERAL" {
  description = "Display name for a billing account"
  type = string
  default = "My Billing Account"
}