provider "google" {
  access_token = var.GCP_ACCESS_TOKEN 
}

provider "google-beta" {
  access_token = var.GCP_ACCESS_TOKEN  
}

# Creates our initial vault module and vault project
module "vault_project" {
  source = "git::https://gitlab.com/iokatech/terraform-modules/terraform-vault-on-cloudrun"

  VAULT_PROJECT_NAME = "{{cookiecutter.org_project_name}} - Vault"
  GCP_ACCESS_TOKEN = var.GCP_ACCESS_TOKEN
  VAULT_DOMAIN = "{{cookiecutter.vault_domain_name}}"
  DOMAIN = var.DOMAIN
  BILLING_NAME_GENERAL = var.BILLING_NAME_GENERAL
}
