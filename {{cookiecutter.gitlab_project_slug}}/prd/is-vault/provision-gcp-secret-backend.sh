#!/bin/sh

while ! vault status; do
  sleep 1
  echo "Checking if Vault is unsealed.  This should happen after completing init in the UI"
done

echo "Login to Vault using the root token you just downloaded"
vault login

vault secrets enable -path=org/gcp gcp
vault write /org/gcp/config ttl=300
vault write /org/gcp/config max_ttl=300

vault write org/gcp/roleset/org-owner \
    project="$VAULT_PROJECT_ID" \
    secret_type="access_token"  \
    token_scopes="https://www.googleapis.com/auth/cloud-platform" \
    bindings=-<<EOF
      resource "//cloudresourcemanager.googleapis.com/organizations/$ORG_ID" {
        roles = [
          "roles/owner",
          "roles/resourcemanager.organizationAdmin",
          "roles/resourcemanager.folderAdmin",
          "roles/iam.securityAdmin",
          "roles/resourcemanager.projectCreator",
          "roles/billing.admin"
        ]
      }
EOF
