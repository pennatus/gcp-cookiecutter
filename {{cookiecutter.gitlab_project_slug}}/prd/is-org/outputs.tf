output "org_project_id" {
  value = module.org_project.org_project_id
  description = "This is the full project id of the main org project"
}