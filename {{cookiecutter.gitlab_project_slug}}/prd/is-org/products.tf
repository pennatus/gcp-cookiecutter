## Declare one of these blocks for each product
# module "django_server_product" {
#   source = "git::https://gitlab.com/pennatus/terraform-modules/terraform-product-factory"

#   MONITORING_PROJECT_ID = module.org_project.org_project_id
#   PRODUCT_NAME = "Django-Server"
#   BILLING_DISPLAY_NAME = var.BILLING_NAME_GENERAL
#   DOMAIN = var.DOMAIN
#   FOLDER_IDS = module.org_project.folder_ids
#   ENVIRONMENTS = [ "prd" ]
#   APIS = []
#   SSH_KEYS = []
#   KV_SECRET_PATHS = []
# }