# Infrastructure for global GCP resources

The global infrastructure is responsible for spawning new projects and managing resources 
that affect the GCP organization.  Shared resources should be placed in a separate project
with less permissions.

## I/O

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| DOMAIN | This is the domain for which your GCP account is hosted under.  Supplied by CookieCutter template. | string | n/a | yes |
| VAULT\_ROOT\_TOKEN | Root Vault Token - should only use this once to setup the initial policies and then use the helper scripts to generate a new approle roleid/secretid pair | string | n/a | yes |
| BILLING\_NAME\_GENERAL | Display name for a billing account | string | `"My Billing Account"` | no |
| ENVIRONMENTS | List of possible environments for all projects | list | `[ "dev", "stg", "prd" ]` | no |
| OIDC\_CLIENT\_ID | After setting up access credentials in is-org this is the OIDC Client ID for the application.  Specify this if you want to offer your users OIDC login access. | string | `""` | no |
| OIDC\_CLIENT\_SECRET | After setting up access credentials in is-org this is the OIDC Client Secret for the application.  Specify this if you want to offer your users OIDC login access. | string | `""` | no |
| OIDC\_DEFAULT\_ROLE | This is the default Vault Policy to give users that successfully authenticate using OIDC. | string | `"team"` | no |
| VAULT\_APPROLE\_ROLE\_ID | Use the AppRole RoleId/SecretId pair once you have the initial policies configured. | string | `""` | no |
| VAULT\_APPROLE\_SECRET\_ID | Use the AppRole RoleId/SecretId pair once you have the initial policies configured. | string | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| org\_project\_id | This is the full project id of the main org project |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
