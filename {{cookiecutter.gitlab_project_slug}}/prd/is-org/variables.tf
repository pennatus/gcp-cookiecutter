# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be supplied and are secretive
# ---------------------------------------------------------------------------------------------------------------------

variable "VAULT_ROOT_TOKEN" {
  description = "Root Vault Token - should only use this once to setup the initial policies and then use the helper scripts to generate a new approle roleid/secretid pair"
  type        = string
}

variable "VAULT_APPROLE_ROLE_ID" {
  description = "Use the AppRole RoleId/SecretId pair once you have the initial policies configured."
  type        = string
  default = ""
}

variable "VAULT_APPROLE_SECRET_ID" {
  description = "Use the AppRole RoleId/SecretId pair once you have the initial policies configured."
  type        = string
  default = ""
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "DOMAIN" {
  description = "This is the domain for which your GCP account is hosted under.  Supplied by CookieCutter template."
  type        = string
}

variable "OIDC_CLIENT_ID" {
  description = "After setting up access credentials in is-org this is the OIDC Client ID for the application.  Specify this if you want to offer your users OIDC login access."
  type        = string
  default     = ""
}

variable "OIDC_CLIENT_SECRET" {
  description = "After setting up access credentials in is-org this is the OIDC Client Secret for the application.  Specify this if you want to offer your users OIDC login access."
  type        = string
  default     = ""
}

variable "OIDC_DEFAULT_ROLE" {
  description = "This is the default Vault Policy to give users that successfully authenticate using OIDC."
  type        = string
  default     = "team"
}

variable "ENVIRONMENTS" {
  description = "List of possible environments for all projects"
  type = list
  default = ["dev", "stg", "prd"]
}

variable "BILLING_NAME_GENERAL" {
  description = "Display name for a billing account"
  type = string
  default = "My Billing Account"
}