terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.28.0, < 5.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 4.4.0, < 5.0"
    }
  }
}

# Setup our remote state link so we can pull stuff from the is_vault workspace
data "terraform_remote_state" "is_vault" {
  backend = "remote"

  config = {
    organization = "{{cookiecutter.gitlab_project_slug}}"
    workspaces = {
      name = "prd-is-vault"
    }
  }
}

provider "vault" {
  address = data.terraform_remote_state.is_vault.outputs.vault_url
  token = var.VAULT_ROOT_TOKEN
  ##########################################################################################################
  ## Use this method for Vault Auth once you have brought everything up and created an approle and appsecret
  # auth_login {
  #   path = "auth/approle/login"
  #   parameters = {
  #     role_id   = var.VAULT_APPROLE_ROLE_ID
  #     secret_id = var.VAULT_APPROLE_SECRET_ID
  #   }
  # }
}


# We have a secrent engine that can get us a GCP token so let's get it and then we can use it below
data "vault_generic_secret" "gcp_token" {
   path = "/org/gcp/token/org-owner"
}


# Helper to keep track of the GCP token since we use it in a few places.
locals {
  GCP_TOKEN = data.vault_generic_secret.gcp_token.data["token"]
  ZONE = "us-central1-a"
}


provider "google" {
  access_token = local.GCP_TOKEN
  zone = local.ZONE
}

provider "google-beta" {
  access_token = local.GCP_TOKEN
  zone = local.ZONE
}


## Define some common org level resources - i.e. is-org project
module "org_project" {
  source = "git::https://gitlab.com/pennatus/terraform-modules/terraform-org"

  ORG_PROJECT_NAME = "{{cookiecutter.org_project_name}} - Org"
  DOMAIN = var.DOMAIN
  BILLING_NAME_GENERAL = var.BILLING_NAME_GENERAL
  ENVIRONMENTS = var.ENVIRONMENTS
}


## Use this block to map entities to vault policies.  Entities could be things like,
## approle names, gcp project ids, etc..
module "vault_auth" {
  source = "git::https://gitlab.com/pennatus/terraform-modules/terraform-vault-auth"

  APPROLE_VAULTPOLICY_MAP = {
    #"terraform-is-org": [ module.org_project.vault_admin_policies.name ]
  }

  GCP_PROJECT_VAULTPOLICY_MAP = {

  }
}


## Declare one of these blocks for each database instance - these are expensive so consider
## adding more databases to a single instance.
# module "shared_db" {
#   source = "git::https://gitlab.com/pennatus/terraform-modules/terraform-postgres-db"

#   ZONE = local.ZONE
#   PROJECT_ID = module.org_project.org_project_id
#   NAME = "shared-db"
#   DATABASES = [ "mydatabase1" ]
#   GCP_TOKEN = local.GCP_TOKEN
#   VAULT_PROJECT_ID = data.terraform_remote_state.is_vault.outputs.vault_project_id
#   VAULT_ADDRESS = data.terraform_remote_state.is_vault.outputs.vault_url
#   VAULT_TOKEN = var.VAULT_ROOT_TOKEN
#   # DELETION_PROTECTION = false - uncomment if you want to delete
# }