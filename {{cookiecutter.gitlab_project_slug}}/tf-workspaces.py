import logging
import os

import click
from pyterprise import Client
from hvac import Client as VaultClient
import pyterprise

token=os.environ['TFC_API_KEY']
host=os.environ.get('TFC_HOST',"https://app.terraform.io")


client = Client()
client.init(token=token, url=host)
log = logging.getLogger('tf_workspace')

@click.group()
@click.option('--debug/--no-debug', default=False)
def cli(debug):
  log_level=logging.INFO
  if debug:
    log_level = logging.DEBUG

  logging.basicConfig(level=log_level)

@cli.command()
@click.argument('org_name', default="{{cookiecutter.gitlab_project_slug}}")
@click.argument('is_workspace')
@click.argument('suffix')
@click.option('--cloudrun',is_flag=True, help="Sets an additional Terraform variable: DEPLOYED_VERSION")
@click.argument('environments', nargs=-1)
def create(org_name, is_workspace, suffix, cloudrun, environments):
  '''
  Creates a dev, stg and prd workspace in terraform.io with
  SUFFIX for each environment in the organization
  specified by ORG_NAME.  Uses IS_WORKSPACE to read and connect
  the VCS credentials for each environment.

  Arguemnts:

    ORG_NAME: Terraform.io organization name, case sensitive

    IS_WORKSPACE: Workspace name of the org infrastructure, req'd to setup VCS

    SUFFIX: Usually the name of the product, ex: stg-name

    ENVIRONMENTS: Name of each env to create, ex: prd stg

  Environment variables:

    VAULT_TOKEN: Used by Vault client if found, otherwise uses current vault token. Use `vault login` to get a new token if you need one

    VAULT_ADDR:  Used by Vault client to determine Vault location

    TFC_API_KEY: Must be set to authenticate with terraform.io, API key can be created from web ui

    TFC_HOST: The address to the terraform.io, defaults to https://app.terraform.io
  '''
  org = client.set_organization(id=org_name)
  # if tf_version:
  #   kwargs = { "tf_version": tf_version }

  is_org_workspace = org.get_workspace(is_workspace)
  vault_client = VaultClient()

  # Check that there is an AppRole for each environment
  for environment in environments:
    tf_approle_name = f'terraform-{suffix}-{environment}'
    role_id=vault_client.read(f'/auth/approle/role/terraform-{suffix}-{environment}/role-id')
    if not role_id:
      raise RuntimeError(f"Must create an AppRole mapping () in infrastructure workspace for `{tf_approle_name}``")

  for environment in environments:
    workspace = f"{environment}-{suffix}"
    prefix=f"/{environment}/{suffix}"
    log.debug(f"Using {prefix} tto")
    vcs_info={
                "identifier": is_org_workspace.vcs_repo.identifier,
                "oauth-token-id": is_org_workspace.vcs_repo.oauth_token_id,
                "branch": is_org_workspace.vcs_repo.branch,
                "default-branch": True,
    }
    role_id=vault_client.read(f'/auth/approle/role/terraform-{suffix}-{environment}/role-id')['data']['role_id']
    secret_id=vault_client.write(f'/auth/approle/role/terraform-{suffix}-{environment}/secret-id')['data']['secret_id']
    try:
      org.get_workspace(workspace)
      org.delete_workspace(workspace)
    except pyterprise.exceptions.APIException:
      pass

    # org.create_workspace(workspace,tf_version=tf_version,working_directory=environment,vcs_repo=vcs_info,trigger_prefixes=[f"/{environment}/{suffix}"])
    org.create_workspace(workspace,tf_version="0.12.28",working_directory=prefix,vcs_repo=vcs_info,trigger_prefixes=[f"/modules"])
    new_workspace = org.get_workspace(workspace)
    new_workspace.create_variable("VAULT_APPROLE_SECRET_ID", secret_id, category="terraform", sensitive=True)
    new_workspace.create_variable("VAULT_APPROLE_ROLE_ID", role_id, category="terraform")
    if cloudrun:
      new_workspace.create_variable("DEPLOYED_VERSION", "latest", category="terraform")

if __name__ == "__main__":
  cli()