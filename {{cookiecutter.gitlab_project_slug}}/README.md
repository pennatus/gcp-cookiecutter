# {{cookiecutter.org_name}}

## Setup

At this point you have run Cookie Cutter and have customized the project to your needs.  The next step is to setup accounts/organizations for each service you need.

### GCP

Disclaimer: Every effort is made to not incur GCP charges, but pricing methodology changes over time and this project could incur charges to your account.

1. Procure a domain name, ex: `awesome-project.com`
1. Create a new Cloud Identity by visiting <https://workspace.google.com> and click the Get Started button.  From here, simply follow the instructions to setup your free GCP Cloud Identity.
1. Follow the steps to register your domain with Cloud Identity
1. You will need permissions to bootstrap the organisation in GCP Console.
   1. Go into the Cloud Identity Pnael (`admin.google.com`) and create a `GCP Super Admin` group.  Remember the email address you created as that will be used in the GCP Console.
   1. Add yourself to this group.  Note: You may want to lower your permissions after you have finished bootstrapping.
   1. Navigate to the GCP Console (`console.cloud.google.com`) and go to the IAM tool.  Create a new member for the super admin account using `Grant Access` and assign the following roles (IAM & Admin -> IAM -> Add) to the email address of the `GCP Super Admin` group:
      1. `Project Creator`
      1. `Billing Account Administrator`
      1. `Folder Admin`
      1. `Organization Administrator`
   1. Remove the 2 default permissions (`Project Creator` and `Billing Account Creator`) from the organization as these are now granted with the super admin group.
1. Link a billing account to this organization (Billing->Add Billing Account)

You now have a new GCP console associated with your domain that has a root Organization and a Seed Account (your account) which allows the creation of Folders and the assignment of additional roles from the root through each folder level.  **Pro Tip: Use Chome profiles to keep your personal and organization login separate to avoid confusing the GCP console**

Before continuing, review this link [default access control](https://cloud.google.com/resource-manager/docs/default-access-control) which explains how to remove the organisation permissions so they don't automatically inherit in all project service accounts.  Otherwise the Product Service Accounts would inherit `Organisation Administrator` giving them access to escalating their own permissions.

Also consider reviewing [using groups and service accounts](https://cloud.google.com/docs/enterprise/best-practices-for-enterprise-organizations#groups-and-service-accounts) to separate duties and assign backups to users using groups instead of directly assigning roles to users.

### Gitlab.com

Next step is to create a new Gitlab account that will just have access to your infrastructure.  The
idea being that you don't want to give out your personal account which may have access to many
other Gitlab projects.

1. Sign out and create a new account named `awesome-project-sa`.  When asked for an email address you may need to use something
   like `your_personal_email_user+<something>@yourdomain.com`.  The `+<something>` allows you to use a 'unique' address but
   still receive the email your your personal email account.
1. Sign back in as yourself and add the user `awesome-project-sa` to your `infrastructure` subgroup as a `Maintainer` role.

You now have a service account that has permissions to only the projects you wish to share with it.  This way you don't have to use your personal account which may have permissions to projects/groups unrelated to `awesome-project`.

### [Terraform.io](https://terraform.io)

If you don't have an account for terraform.io, you can you create a personal account similar to Gitlab.  The structure of terraform.io is that you can be a collaborator to other teams, create multiple organizations and even associate separate repositories to each workspace.  Therefore you should not need a separate account for each organization you work with.

NOTE: Before doing this step, setup the DNS records for the `{{cookiecutter.vault_domain_name}}` domain you chose when running cookiecutter.  This will ensure a smooth start to the DNS mapping to in GCP.

For example, to have the address `https://vault.mysite.com` resolve, you will need to add a CNAME record
with the host set to `vault` and the data set to `ghs.googlehosted.com.`.

1. Once you've created your account, log-in and create an organization for `{{cookiecutter.org_name}}`.
1. Setup a VCS Provider (Settings -> Version Control -> Providers -> Add a VCS Provider)
    1. Select Gitlab
    1. Follow the step by step instructions
    1. When authenticating with Gitlab, use the "Service Account" created above.  This ensures that Terraform only has access to one repository (infrastructure).    
1. Create a workspace for `{{cookiecutter.org_infrastructure_workspace_name}}`
    1. Monitor `/prd/is-org` directory for changes
    1. Use the default branch
    1. In General -> Settings update the access control to share state with all workspaces in the organization
1. Create a workspace for `prd-is-vault`
    1. Monitor `/prd/is-vault` directory for changes
    1. Use the default branch
    1. In General -> Settings update the access control to share state with all workspaces in the organization

At this point we're very close!  We will log into GCP console from the command line to obtain an access token which will bootstrap the `is-org` and `is-vault` infrastructure.  Later on that root key will be removed and replaced by Vault credentials

It's now time to run your first Terraform Plan on your organization which will initialize the GCP project that holds Vault:

1. Install `GCloud SDK` and then authenticate with GCP: `gcloud auth login`
1. Run `gcloud auth print-access-token`  to get a short lived GCP access token that will be used by Terraform once.
1. For the Vault Terraform workspace click on `variables` and add a new `Terraform Variable` called `GCP_ACCESS_TOKEN` and set it to the value from the previous step.  Make sure to mark the variables as `sensitive`.
1. Add another variable to the `is-org` workspaces called `VAULT_ROOT_TOKEN`, it does not need a value right now because it is not used.

At this point you now have a Hashicorp Vault instance running in Google's Cloud Run.  Vault requires initialization.  This can be done by first changing into the `prd/is-vault` folder and then from the command line using `make ui`.  From here set the number of unseal keys required and the threshold.  Normally, this is set to 5 unseal keys with a threshold of 3.  Click `Initialize` and then `Download Keys` to finish.  Your Vault is now initialized and ready to use.  Keep the downloaded file in a safe place and make a note of the initial root token.  You will need the root token in the next step to setup Vault.

### Vault Setup - Production Infrastructure workspace

Now that the Vault workspace in Terraform has successfully applied a plan the next step is to configure the Vault instance using the `is-org` workspace.  For the next steps you will edit the terraform code in `is-org` and be working that workspace in terraform.io.

1. Create a variable called `VAULT_ROOT_TOKEN` and set the value to your initial Vault root token from the previous section.  Make sure to set this as sensitive.
1. Queue a plan.
1. Confirm & Apply the plan.

Congratulations! Your Vault instance can now issue GCP tokens and manage secrets for projects.

### Vault Setup - Tuning

The next goal is to remove the root token from use (consider rotating it) and replace with an AppRole credential and setup OIDC so members of the Cloud Identity organization can be issued Vault policies to administer or use Vault and tune some settings.

Edit the `prd/is-org/main.tf` and uncomment the line in the

``` hcl
"terraform-is-org": [ module.org_project.vault_admin_policies.name ]
```

Commit and push the above change and after the Terraform plan is successfully applied, 
install the Vault client for your OS and run the following:

``` sh
export VAULT_ADDR=https://{{cookiecutter.vault_domain_name}}
VAULT_TOKEN=<root vault_token> vault read  auth/approle/role/terraform-is-org/role-id
VAULT_TOKEN=<root vault_token> vault write -f  auth/approle/role/terraform-is-org/secret-id
```

Use the `secret-id` to populate a variable in the is-org workspace called `VAULT_APPROLE_SECRET_ID` and use the output from `role-id` to populate `VAULT_APPROLE_ROLE_ID`.  Remember to mark the secret id variable as "sensitive".  Edit the `VAULT_ROOT_TOKEN` variable and set the value to "not_used" to invalidate the token.

In `prd/is-org/main.tf` uncomment:

``` hcl
  auth_login {
    path = "auth/approle/login"
    parameters = {
      role_id   = var.VAULT_APPROLE_ROLE_ID
      secret_id = var.VAULT_APPROLE_SECRET_ID
    }
  }
```

and delete

``` hcl
   token = var.VAULT_ROOT_TOKEN
```

Commit and push this change.  If this successfully plans then AppRole is now configured for Terraform.

Additional app roles can be created and mapped to policies by creating new app roles with the following:

``` sh
export VAULT_ADDR=https://{{cookiecutter.vault_domain_name}}
VAULT_TOKEN=<root vault_token> vault read  auth/approle/role/<new role>/role-id
VAULT_TOKEN=<root vault_token> vault write -f  auth/approle/role/<new role>/secret-id
```

You then add entries inside the vault auth module like the following:

``` hcl
module "vault_auth" {
  ...
  APPROLE_VAULTPOLICY_MAP = {    
    ...
    "<new role>": [ <list of vault policies to grant> ]>
    ...
  }
  ...
}
```

### Setup OIDC credentials (allow members Cloud Identity org to log into Vault)

This section is useful if you want to use Vault to manage secrets that the entire team may need to access.  You can define policies per member or per group such that any member of the Cloud Idenity/GSuite organization can be granted access to various secret engines in Vault.  The most common use case would be administrators of the GCP console who would have access to all areas.

1. In the GCP Console, for the IS-Vault project, go to the `APIs & Services -> Credentials` page and click on the `Configure consent screen`.
    1. Select Internal Application Type - this restricts auth to members in your domain.
    1. Choose an `Application name` - i.e. `Vault Auth Client`.  Add your domain to the `Authorized domains`.
    1. Click `Save`.
1. Click back to `Credentials` again and click `Create credentials` and choose `OAuth client ID` from the drop down.
    1. Choose `Web application` for the Application Type.
    1. Provide a name like `Vault Client`.
    1. Add `Authorized redirect URIs`
        1. Add `https://{{cookiecutter.vault_domain_name}}/ui/vault/auth/oidc/oidc/callback`
        1. Add `http://localhost:8250/oidc/callback`.
        1. NOTE: the duplicate `oidc` in the URI is not a typo.  It is because the auth provider is mounted at the `/oidc` path.
    1. Click `Create`.
1. Take note of the client id and secret that will show up in the completion popup.

To configure Vault for OIDC the credentials will be ENV variables in terraform.io and `is-org` terraform code needs to be modified

1. In `prd-is-org` workspace in terraform.io
    1. Add client id from above to `OIDC_CLIENT_ID` as a Terraform Variable
    1. Add secret id from above to `OIDC_CLIENT_SECRET` as a `sensitive` Terraform variable.

Next, you need to configure the `vault_auth` module in `prd/is-org/main.tf` by adding the following snippet inside the braces:
``` hcl
  module "vault_auth" {
    ...
      
    OIDC_VAULTPOLICY_MAP = {
      "${var.OIDC_DEFAULT_ROLE}": [ ] # List vault policies here that users get when logged in with oidc
    }

    OIDC_DEFAULT_ROLE  = var.OIDC_DEFAULT_ROLE
    OIDC_CLIENT_ID     = var.OIDC_CLIENT_ID
    OIDC_CLIENT_SECRET = var.OIDC_CLIENT_SECRET

    # This must match the domain mapped ot the CloudRun Vault service, otherwise Vault will reject as unauthorized redirect_uri.
    VAULT_MANAGED_ADDRESS = "https://{{cookiecutter.vault_domain_name}}"
  }
```

Commit and push, then review the plan in terraform.io (1 resource to add) and apply.

At this point you should be able to either go to the Vault UI at `https://{{cookiecutter.vault_domain_name}}`, select OIDC as the authentication method and click "Google".  Login using your account registered to `<myorg.com>` and you will be allowed into Vault.

You can also get a login via the Vault CLI and get a token using: `vault login -method=oidc`

***Congratulations!!!***  You now have a Terraform Workspace, GCP Project and a Vault instance deployed that will allow members of your GCP organization to authenticate.  This particular workspace should be considered special as it is the parent for all projects and controls the foundation of your GCP org.  You're now ready for the next step which is to add the first Product which is a GCP project that holdes shared resources for your organization and keeps them separate from the IS-Org Product.  The next few sections will cover some more details and provide an example.

## Advanced Topics

After getting Vault and the basic organization under Terraform where you go next is entirely up to you.  The Pennatus modules help configure integrate Vault & GCP for things like policy mapping or creating additional projects in both GCP and Vault.  The next sections are good places to start as examples.

## Org policies

The `org-policies` module has a layout for default policies.  These policies are required for the Product Factor Module (PFM), but should be reviewed to understand what is being granted as they generally enable access to a GCP roleset.  For example, a `product-prd-website-viewer` could be issued to a network admin to be able to view logs and monitor the production website.  The policies can also be used to form groups in vault to make team based roles for users that authenticate with OIDC.

The Product Owner policy provides an easy way to grant access to Product Workspaces in terraform.io for managing GCP resources.  The first product example, "Shared", will use this default policy to lower the permissions of the Terraform `prd-shared` workspace to prevent unauthorzied/accidental changes to the GCP organisation and Vault configuration.

## Your First Product: Shared

The `is-org` product is a special because the GCP permissions granted to terraform.io are much greater.  This product will be the first example of a typical product of which you will likely have many.  The `shared` product is an example for managing global GCP resources that have "team" read access.  This example will illustrate Vault policy management and adding new projects.  Feel
free to name this project as you want, just change all instances of `shared` with whatever name you choose.

Navigate to the `prd/is-org` folder and edit the `products.tf` file.  There is a sample Product commented ou, but we will create a new one from the example below.  See [PFM Variables](modules/product-factory/variables.tf) for detailed documentation on each variable to understand what this will do.

```hcl
module "product_shared" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/product-factory"

  PRODUCT_NAME = "shared"
  ENVIRONMENTS = [ "prd" ]
  APIS = [ "containerregistry.googleapis.com", "storage-component.googleapis.com" ]
  SSH_KEYS = [ ]
  KV_SECRET_PATHS = [ ]

  ADMIN_PROJECT = var.ADMIN_PROJECT
  BILLING_DISPLAY_NAME = var.BILLING_NAME_GENERAL
  DOMAIN = var.DOMAIN
  FOLDER_IDS = module.org_folders.folder_ids
}

output project_ids {
  value = module.product_shared.env_project_map
}
```

1. Add new entries into the `vault_auth` module in main.tf:

```hcl
module "vault_auth" {
  ...

  APPROLE_VAULTPOLICY_MAP = {
    ...
    "terraform-shared-prd": [ module.product_shared.vault_owner_policies.prd.name,
                              module.org_project.vault_token_creator_policies.name ]
  }

  ...
}
```

Push the changes to Gitlab and this will trigger a Plan on Terraform.
Look at the changes and then Apply the plan.  This will create your new GCP project and setup Vault Polices that you can
now assign to users.

1. The example only created one environment, so at the root `mkdir prd/shared` is sufficient.
1. Create the base product terraform files, but no content: `touch prd/shared/{main,variables,outputs}.tf`.
1. Go to terraform.io and add a new workspace to your organization.  Name it `prd-shared` and configure the monitored directories as `prd/shared`.
1. You can now create a new APPROLE Vault token using the following:

```sh
VAULT_ADDR=https://vault.pennatus.ioka.app VAULT_TOKEN=<root token> vault read  auth/approle/role/terraform-shared-prd/role-id
VAULT_ADDR=https://vault.pennatus.ioka.app VAULT_TOKEN=<root token> vault vault write -f  auth/approle/role/terraform-shared-prd/secret-id
```

1. Use Terraform environment variables similar to what was done for the `is-org` workspace for setting up authentication in the new TF `shared` workspace.
1. Add the initial contents of your `main.tf` in the new `prd/shared/main.tf` file.  Something similar to the following will get you started.

```hcl
# Setup our remote state link so we can pull stuff from the is_vault workspace
data "terraform_remote_state" "is_vault" {
  backend = "remote"

  config = {
    organization = "{{cookiecutter.gitlab_project_slug}}"
    workspaces = {
      name = "prd-is-vault"
    }
  }
}

provider "vault" {
  address = data.terraform_remote_state.is_vault.outputs.vault_url
  # Use this method for Vault Auth once you have brought everything up and created an approle and appsecret
  auth_login {
    path = "auth/approle/login"
    parameters = {
      role_id   = var.VAULT_APPROLE_ROLE_ID
      secret_id = var.VAULT_APPROLE_SECRET_ID
    }
  }
}


# We have a secrent engine that can get us a GCP token so let's get it and then we can use it below
# NOTE: The `shared` reference below needs to match the product name you chose
data "vault_generic_secret" "gcp_token" {
   path = "org/gcp/token/prd-shared-ow"
}


# Helper to keep track of the GCP token since we use it in a few places.
locals {
  GCP_TOKEN = data.vault_generic_secret.gcp_token.data["token"]
  ZONE = "us-central1-a"
}


provider "google" {
  access_token = local.GCP_TOKEN
  zone = local.ZONE
}

provider "google-beta" {
  access_token = local.GCP_TOKEN
  zone = local.ZONE
}
```

1. Push changes to Gitlab to trigger a plan/apply on the new `prd-shared` workspace.

## Next steps

At this point you should be able to manage your organization by writing Terraform to configure resources along and adding Vault config as demonstrated above.  Here is a list of additional areas to review:

1. Manage all your domain records by using GCP Cloud DNS servers.  The umbrella resource could be configured in the Shared product with each Product adding DNS records (resources) if the Product Owner SA is granted the role.
1. Manage internal resources used by more than one project, for example a shared Docker registry or a private PyPi registry.  Note, larger shared projects will likely justify being a separate product so that changes to the Shared Product workspace don't trigger Plan/Review/Apply on critical resources.
1. Create a new Product for a specific application like a website or webapp using the Product Factory Module (PFM).
1. Create a shared database server usable by multiple projects: [PostgresQL on GCP](modules/postgres-db/variables.tf)

Generally changes will fall into one or more categories:

- Create/Delete/Update a set of Product resources in Terraform for each product environments
  - GCP resources
  - Vault paths (Secret Engine mounts, or additional paths specific to a Secret Engine)
- Create/Delete/Update Organisaton (`org-is` product) resources
  - Vault policies to provide product access
  - GCP projects (using PFM)
  - Additional Vault auth endpoints

This allows apps (or any client) to access Vault with a scoped token and read any secrets that are needed.
